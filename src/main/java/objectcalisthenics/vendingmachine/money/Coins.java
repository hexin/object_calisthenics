package objectcalisthenics.vendingmachine.money;

import java.util.ArrayList;
import java.util.List;

public class Coins {

    private List<Coin> coins = new ArrayList<Coin>();

    public void add(Coin coin) {
        coins.add(coin);
    }

    public Money summedValue() {
        Money sum = new Money(0);
        for (Coin coin : coins) {
            sum = sum.add(coin);
        }
        return sum;
    }

    @Override
    public String toString() {
        return coins.toString();
    }
}