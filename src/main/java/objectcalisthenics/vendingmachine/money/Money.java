package objectcalisthenics.vendingmachine.money;

public class Money {

    private int value;

    public Money(int value) {
        this.value = value;
    }

    public Money add(Money money) {
        return new Money(this.value + money.value);
    }

    public boolean lessOrEqual(Money that) {
        return this.value <= that.value;
    }

    public boolean hasSameValue(Money that) {
        return this.value == that.value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        return hasSameValue((Money) object);
    }

    @Override
    public int hashCode() {
        return value;
    }
}
