package objectcalisthenics.vendingmachine.money;

public enum Denominator {

    CENTS_500(5),
    CENTS_200(2),
    CENTS_100(1);

    public int value;

    private Denominator(int value) {
        this.value = value;
    }

    public static boolean isSmallest(Denominator denominator) {
        return denominator == CENTS_100;
    }

    public static Denominator greatest() {
        return CENTS_500;
    }

    public Denominator next() {
        if (isSmallest(this))
            return this;
        return values()[ordinal() + 1];
    }
}

